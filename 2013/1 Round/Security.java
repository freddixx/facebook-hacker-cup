import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Security {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		Security p = new Security();
		p.solve();
	}

	private void solve() {
		try {
			String infile = "security.txt";
			String outfile = "output.txt";
			List<Keyset> contents = readContents(infile);
			int counter = 1;
			PrintWriter writer;
			writer = new PrintWriter(outfile);
			for (Keyset k : contents) {
				writer.print("Case #" + counter + ": ");
				String[] solution = matchKeys(k);
				String output = "";
				for (String s : solution) {
					if (s == null) {
						output = "IMPOSSIBLE";
						break;
					}
					output += s;
				}
				writer.print(output);
				writer.print("\n");
				writer.flush();
				counter++;
			}
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String[] matchKeys(Keyset k) {
		String[] matchedKey = new String[k.m];
		int currentPtr = 0;
		for (String s : k.k1) {
			int maxScore = 0;
			int matchIdx = 0;
			for (int j = 0; j < k.k2.size(); j++) {
				int score = checkFitness(s, k.k2.get(j));
				// The parts do not match
				if (score == 0)
					continue;
				// The parts do match in some kind
				if (score > 0 && score > maxScore) {
					String restoredKey = restoreKeySegment(s, k.k2.get(j));
					if (!restoredKey.equals("")) {
						matchIdx = j;
						matchedKey[currentPtr] = restoredKey;
						maxScore = score;
					}
				}
			}
			if (maxScore == 0) {
				break;
			} else {
				k.k2.remove(matchIdx);
				currentPtr++;
			}
		}
		return matchedKey;
	}

	private String restoreKeySegment(String s1, String s2) {
		String s = "";
		for (int i = 0; i < s1.length(); i++)
			if (s1.charAt(i) == '?' && s2.charAt(i) != '?')
				s += s2.charAt(i);
			else if (s1.charAt(i) != '?' && s2.charAt(i) == '?')
				s += s1.charAt(i);
			else if (s1.charAt(i) == '?' && s2.charAt(i) == '?')
				s += "a";
			else if (s1.charAt(i) == s2.charAt(i))
				s += s1.charAt(i);
		return s;
	}

	private int checkFitness(String s1, String s2) {
		int score = 0;
		for (int i = 0; i < s1.length(); i++) {
			if ((s1.charAt(i) != '?' && s2.charAt(i) != '?')
					&& s1.charAt(i) != s2.charAt(i))
				return 0;
			if (s1.charAt(i) == '?' && s2.charAt(i) == '?')
				score += 1;
			else if (s1.charAt(i) == '?' && s2.charAt(i) != '?')
				score += 2;
			else if (s2.charAt(i) == '?' && s1.charAt(i) != '?')
				score += 2;
			else if (s2.charAt(i) == s1.charAt(i))
				score += 3;
		}
		return score;
	}

	private List<Keyset> readContents(String file) {
		List<Keyset> contents = new LinkedList<Keyset>();
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line = null;
			int counter = -1;
			String k1 = "", k2 = "";
			int m = 0;
			while ((line = reader.readLine()) != null) {
				if (counter == 0)
					m = Integer.valueOf(line);
				else if (counter == 1)
					k1 = line;
				else if (counter == 2) {
					k2 = line;
					Keyset k = new Keyset(m, k1, k2);
					contents.add(k);
					counter = -1;
				}
				counter++;
			}
		} catch (IOException x) {
			System.err.println(x);
		}
		return contents;
	}

	class Keyset {
		public int m = 1;
		public List<String> k1 = new ArrayList<String>();
		public List<String> k2 = new ArrayList<String>();
		public String rawk1;
		public String rawk2;

		public Keyset(final int m, String k1, String k2) {
			this.m = m;
			this.rawk1 = k1;
			this.rawk2 = k2;
			final int l = k1.length() / m;
			// Do k1
			for (int i = 0; i < k1.length(); i += l) {
				String part = k1.substring(i, i + l);
				this.k1.add(part);
				if (i + l >= k1.length() - 1)
					break;
			}
			// Do k1
			for (int i = 0; i < k2.length(); i += l) {
				String part = k2.substring(i, i + l);
				this.k2.add(part);
				if (i + l >= k2.length() - 1)
					break;
			}
		}

	}

}
