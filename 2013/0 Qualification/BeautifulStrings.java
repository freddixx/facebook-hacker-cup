import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BeautifulStrings {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BeautifulStrings bs = new BeautifulStrings();
		bs.solve();
	}

	public void solve()  {
		try {
			String infile = "beautiful_stringstxt.txt";
			String outfile = "output.txt";
			PrintWriter writer = new PrintWriter(outfile);
			String[] contents = readContents(infile);
			for (int i = 0; i < contents.length; i++) {
				int b = getStringBeauty(contents[i]);
				writer.println("Case #" + (i + 1) + ": " + b);
			}
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private int getStringBeauty(String s) {
		// Tidy up string
		s = s.toLowerCase();
		s = s.replaceAll("[^a-z]", "");
		// Prepare the letters
		List<Letter> letters = new LinkedList<Letter>();
		int beauty = 0;
		int maxBeauty = 26;
		for (int i = 97; i < 123; i++) {
			Letter l = new Letter((char) i);
			letters.add(l);
		}
		// Do the calculation
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			letters.get(c - 97).countUp();
		}
		Collections.sort(letters);
		for (Letter l : letters) {
			beauty += l.getCount() * maxBeauty;
			maxBeauty--;
		}
		return beauty;
	}

	private String[] readContents(String file) {
		String[] contents = null;
		try (
			BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line = null;
			int counter = 0;
			while ((line = reader.readLine()) != null) {
				if (contents == null) {
					contents = new String[Integer.valueOf(line)];
					continue;
				}
				contents[counter] = line;
				counter++;
			}
		} catch (IOException x) {
			System.err.println(x);
		}
		return contents;
	}
	
	class Letter implements Comparable<Letter> {
		private int count = 0;
		private char c;
		
		public Letter (char c) {
			this.c = c;
		}
		
		public int getCount(){
			return this.count;
		}
		
		public void countUp(){
			this.count++;
		}

		@Override
		public int compareTo(Letter l) {
			return Integer.compare(l.count,this.count);
		}
		
		public String toString(){
			return this.c + ": " + this.count;
		}
		
		
	}
}
